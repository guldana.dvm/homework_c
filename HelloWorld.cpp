﻿// HelloWorld.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include "Helpers.h"

int main()
{
    std::cout << "Hello World!\n";

    int a = 3;
    int b = 4;
    int result = squareOfSum(a, b);
    std::cout << "Square of sum: " << result << std::endl;
    result = 0;
}